import os
import requests
from datetime import datetime
import re
from packaging import version
from typing import List, Dict, Optional, Callable, Any

GITLAB_URL = "https://gitlab.com"
GITLAB_PROJECT_ID = f"quantify-os%2F{os.environ.get('CI_PROJECT_NAME')}"
# GITLAB_TOKEN = os.environ.get("CI_JOB_TOKEN")


LABEL_MAPPINGS = {
    "bug fix": "🐛 Bug Fixes and Closed Issues",
    "new feature": "✨ New Features",
    "breaking change": "💥 Breaking Changes",
    "enhancement": "🚀 Enhancements",
    "documentation": "📚 Documentation",
    "refactor": "♻️ Refactoring",
    "dependency": "📦 Dependencies",
    "other": "🔧 Other",
}


# def get_gitlab_api_headers():
#     if not GITLAB_TOKEN:
#         print("Error: GITLAB_TOKEN environment variable not set.")
#         sys.exit(1)
#     return {"PRIVATE-TOKEN": GITLAB_TOKEN}


def make_api_request(
    endpoint: str, params: Optional[Dict[str, Any]] = None
) -> Dict[str, Any]:
    """
    Make an API request to the GitLab API.

    Parameters
    ----------
    endpoint : str
        The API endpoint to request.
    params : Optional[Dict[str, Any]], optional
        Query parameters for the API request, by default None.

    Returns
    -------
    Dict[str, Any]
        The JSON response from the API.

    Raises
    ------
    requests.HTTPError
        If the API request fails.
    """
    url = f"{GITLAB_URL}/api/v4/projects/{GITLAB_PROJECT_ID}/{endpoint}"
    response = requests.get(url, params=params)
    response.raise_for_status()
    return response.json()


def fetch_paginated_data(
    endpoint: str,
    params: Dict[str, Any],
    condition_func: Optional[Callable[[Dict[str, Any]], bool]] = None,
) -> List[Dict[str, Any]]:
    """
    Fetch paginated data from the GitLab API.

    Parameters
    ----------
    endpoint : str
        The API endpoint to request.
    params : Dict[str, Any]
        Query parameters for the API request.
    condition_func : Optional[Callable[[Dict[str, Any]], bool]], optional
        A function to filter the fetched data, by default None.

    Returns
    -------
    List[Dict[str, Any]]
        A list of fetched and optionally filtered data items.
    """
    all_data = []
    page = 1

    while True:
        params["page"] = page
        data = make_api_request(endpoint, params)

        if not data:
            break

        if condition_func:
            filtered_data = [item for item in data if condition_func(item)]  # type: ignore
            all_data.extend(filtered_data)
            if len(filtered_data) < len(data):
                break
        else:
            all_data.extend(data)

        page += 1

    return all_data


def fetch_tags() -> List[Dict[str, Any]]:
    """
    Fetch all tags from the GitLab repository.

    Returns
    -------
    List[Dict[str, Any]]
        A list of dictionaries containing tag information.
    """
    return make_api_request("repository/tags")  # type: ignore


def is_release_version(tag_name: str) -> bool:
    """
    Check if a tag name represents a release version.

    Parameters
    ----------
    tag_name : str
        The name of the tag to check.

    Returns
    -------
    bool
        True if the tag name represents a release version, False otherwise.
    """
    return re.match(r"^v\d+\.\d+\.\d+$", tag_name) is not None


def get_last_release_date() -> str:
    """
    Get the date of the last release.

    Returns
    -------
    str
        The ISO formatted date string of the last release.
    """
    tags = fetch_tags()
    release_tags = [tag for tag in tags if is_release_version(tag["name"])]

    if not release_tags:
        print("No release tags found. Using a default date.")
        return datetime(2023, 1, 1).isoformat()

    sorted_tags = sorted(
        release_tags, key=lambda t: version.parse(t["name"]), reverse=True
    )

    latest_release = sorted_tags[0]
    release_date = datetime.fromisoformat(
        latest_release["commit"]["created_at"].replace("Z", "+00:00")
    )

    return release_date.isoformat()


def fetch_merge_requests(since_date: str) -> List[Dict[str, Any]]:
    """
    Fetch merge requests merged after a specific date.

    Parameters
    ----------
    since_date : str
        The ISO formatted date string to fetch merge requests from.

    Returns
    -------
    List[Dict[str, Any]]
        A list of dictionaries containing merge request information.
    """
    params = {
        "state": "merged",
        "sort": "desc",
        "order_by": "merged_at",
        "per_page": 100,
    }
    return fetch_paginated_data(
        "merge_requests", params, lambda mr: mr["merged_at"] > since_date
    )


def fetch_closed_issues(since_date: str) -> List[Dict[str, Any]]:
    """
    Fetch issues closed after a specific date.

    Parameters
    ----------
    since_date : str
        The ISO formatted date string to fetch closed issues from.

    Returns
    -------
    List[Dict[str, Any]]
        A list of dictionaries containing closed issue information.
    """
    params = {
        "state": "closed",
        "sort": "desc",
        "order_by": "created_at",
        "per_page": 100,
        "created_after": since_date,
    }
    return fetch_paginated_data(
        "issues", params, lambda issue: issue["closed_at"] > since_date
    )


def generate_changelog_entry(item: Dict[str, Any], item_type: str) -> str:
    """
    Generate a changelog entry for a merge request or issue.

    Parameters
    ----------
    item : Dict[str, Any]
        The merge request or issue data.
    item_type : str
        The type of the item, either "mr" for merge request or "issue" for issue.

    Returns
    -------
    str
        A formatted changelog entry string.
    """
    title = item["title"]
    author = item["author"]["name"]
    item_url = item["web_url"]
    item_iid = item["iid"]
    author_url = item["author"]["web_url"]

    title = title[0].upper() + title[1:]
    symbol = "!" if item_type == "mr" else "#"
    return f"- {title} ([{symbol}{item_iid}]({item_url}) by [@{author}]({author_url}))"


def generate_changelog(
    merge_requests: List[Dict[str, Any]], closed_issues: List[Dict[str, Any]]
) -> None:
    """
    Generate a changelog from merge requests and closed issues.

    Parameters
    ----------
    merge_requests : List[Dict[str, Any]]
        A list of merge request data.
    closed_issues : List[Dict[str, Any]]
        A list of closed issue data.
    """
    changelog_delta = f"## Release {current_release_version()} ({today()})\n\n"
    sections = {label: [] for label in LABEL_MAPPINGS.values()}

    for mr in merge_requests:
        if mr["source_branch"].startswith("release"):
            continue
        labels = mr["labels"] or ["other"]
        for label in labels:
            section = LABEL_MAPPINGS.get(label.lower(), "🔧 Other")
            sections[section].append(generate_changelog_entry(mr, "mr"))

    for issue in closed_issues:
        sections[LABEL_MAPPINGS["bug fix"]].append(
            generate_changelog_entry(issue, "issue")
        )

    for section, entries in sections.items():
        if entries:
            changelog_delta += f"### {section}\n"
            changelog_delta += "\n".join(entries) + "\n\n"

    with open("changelog_delta.md", "w") as f:
        f.write(changelog_delta)


def today() -> str:
    """
    Get today's date in the format YYYY-MM-DD.

    Returns
    -------
    str
        Today's date as a string in the format YYYY-MM-DD.
    """
    return datetime.now().strftime("%Y-%m-%d")


def current_release_version() -> Optional[str]:
    """
    Get the current release version from open merge requests.

    Returns
    -------
    Optional[str]
        The current release version if found, None otherwise.
    """
    params = {
        "state": "opened",
        "labels": "Release",
        "order_by": "created_at",
        "sort": "desc",
        "per_page": 1,
    }
    merge_requests = make_api_request("merge_requests", params)

    if not merge_requests:
        return None

    mr = merge_requests[0]  # type: ignore
    match = re.search(r"(v\d+\.\d+\.\d+)", mr["title"])
    return match.group(1) if match else None


def update_changelog() -> None:
    """
    Update the CHANGELOG.md file with the new changelog entries.
    """
    with open("CHANGELOG.md", "r") as f:
        changelog = f.read()

    with open("changelog_delta.md", "r") as f:
        delta_changelog = f.read()

    def replace(match: re.Match) -> str:
        return delta_changelog + match.group(2)

    # Make the version pattern optional to handle both e.g. v0.7.7 and 0.7.7
    version_str = current_release_version()
    print("version_str", version_str)
    if version_str is not None:
        version_str = version_str.lstrip("v")
        version_str = rf"v?{re.escape(version_str)}"
        pattern = re.compile(
            f"(^##[^\n\r]+?{version_str}.+?)(^##[^\n\r]+?\\d\\.\\d\\.\\d.+?$)",
            re.MULTILINE | re.DOTALL,
        )

        changelog = re.sub(pattern, replace, changelog)

        with open("CHANGELOG.md", "w") as f:
            f.write(changelog)


def main() -> None:
    """
    Main function to generate and update the changelog.
    """
    last_release_date = get_last_release_date()
    merge_requests = fetch_merge_requests(last_release_date)
    closed_issues = fetch_closed_issues(last_release_date)
    generate_changelog(merge_requests, closed_issues)
    update_changelog()


if __name__ == "__main__":
    main()
